const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId
const uri = "mongodb+srv://admin:admin@cluster0.owvcs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();
module.exports = () => {
    const controller = {};
  
    controller.listPayments = (req, res) => {
        client.db("nenapay").collection("payments").find().toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            res.status(200).json(result)
          });
                    
    };

    controller.createPayments = (req, res) => {
        console.log(req.body)
        req.body.created = new Date();
        let payments = client.db("nenapay").collection("payments").insertOne(req.body)
        res.status(200).json(payments)
    };

    controller.findPayment = (req, res) => {
        client.db("nenapay").collection("payments").findOne({_id: new ObjectId(req.params.paymentId)}, function(err, document) {
            res.status(200).json(document)
          })
    };

    controller.updatePayment = (req, res) => {
        delete req.body['_id']
        client.db("nenapay").collection("payments").updateOne({_id: new ObjectId(req.params.paymentId)}, {$set: req.body})
        .then((obj) => {
            res.status(200).json(obj)
        })
    };

    controller.removePayment = (req, res) => {
        client.db("nenapay").collection("payments").deleteOne({_id: new ObjectId(req.params.paymentId)}, function(err, document) {
            res.status(200).json("ok")
          })
    };
  
    return controller;
  }