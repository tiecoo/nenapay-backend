module.exports = app => {
    const controller = require('../controllers/payment')();
  
    app.route('/api/v1/payments')
      .get(controller.listPayments)
      .post(controller.createPayments);
      
    app.route('/api/v1/payments/:paymentId')
        .delete(controller.removePayment)
        .get(controller.findPayment)
        .put(controller.updatePayment);
  }