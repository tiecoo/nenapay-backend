const express    = require('express');
const config     = require('config');
var cors = require('cors')

module.exports = () => {
  const app = express();

  // SETANDO VARIÁVEIS DA APLICAÇÃO
  app.set('port', process.env.PORT || config.get('server.port'));

  app.use(cors())
  // MIDDLEWARES
  app.use(express.json());

  
  require('../api/routes/payment')(app);

  return app;
};
