const express    = require('express');
const bodyParser = require('body-parser');
const config     = require('config/config');

module.exports = () => {
  const app = express();

  // SETANDO VARIÁVEIS DA APLICAÇÃO
  app.set('port', process.env.PORT || config.get('server.port'));
  app.use(cors()); // <---- use cors middleware

  // MIDDLEWARES
  app.use(bodyParser.json());

  app.listen(port, () => {
    console.log(`Servidor rodando na porta ${port}`)
  });

  return app;
};
